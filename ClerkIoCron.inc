<?php

/**
 * Interface ClerkIoCronInterface
 */
interface ClerkIoCronInterface {
  public function feed();
  public function cron_execute();
  public function buildOrders($data);
  public function buildProducts($data);
  public function buildCategories($data);
  public function getOrderProducts($order, $full = FALSE);
}

/**
 * Class ClerkIoCron
 */
class ClerkIoCron implements ClerkIoCronInterface {

  /**
   * Get the product feed file.
   *
   * @return array
   */
  public function feed() {
    $data = $this->file_read($this->get_public_file_path('product-feed.json'));

    if (!$data) {
      $data = array();
      $products = $this->file_read($this->get_public_file_path('products.json'));
      $categories = $this->file_read($this->get_public_file_path('categories.json'));
      $orders = $this->file_read($this->get_public_file_path('orders.json'));

      if (!empty($products)) {
        $data['products'] = $products;
      }

      if (!empty($categories)) {
        $data['categories'] = $categories;
      }

       if (!empty($orders)) {
        $data['sales'] = $orders;
      }

      $this->file_write($this->get_public_file_path('product-feed.json'), $data);
    }

    return $data;
  }

  /**
   * Build the orders JSON.
   *
   * @param $data
   */
  public function buildOrders($data) {
    $phase = $data['phase'];
    $items = $data['items'];
    $filename = $this->get_tmp_file_path('orders.json');

    // Start new temp file.
    if (in_array('start', $phase)) {
      $file = $this->file_write($filename, array());
      if (!$file) {
        watchdog('clerk_io', '@filename could not be written. Aborting build orders.', array('@filename' => $filename), WATCHDOG_ERROR);
        return;
      }
    }

    // Write new content to the file.
    $data = $this->file_read($filename);

    //while($result = $results->fetchAssoc()) {
    foreach ($items AS $item) {
      $order = $this->commerceOrderLoad($item);
      $products = $this->getOrderProducts($order);
      $data[] = array(
        'id' => (int)$item,
        'customer' => (int)$order->uid,
        'time' => (int)$order->changed,
        'products' => $products,
      );
    }

    $this->file_write($filename, $data);

    if (in_array('finish', $phase)) {
      $destination = $this->get_public_file_path('orders.json');
      $status = copy($filename, $destination);
      if (!$status) {
        watchdog('clerk_io', '@filename could not be copied to @destination. Destination file was not updated.', array('@filename' => $filename, '@destination' => $destination), WATCHDOG_ERROR);
        return;
      }

      $this->file_destroy();
    }

    drupal_static_reset();
  }

  /**
   * OVERRIDE THIS TO ADD TRUE PRODUCT FEED.
   *
   * @param $data
   */
  public function buildProducts($data) {
    $phase = $data['phase'];
    $items = $data['items'];
    $filename = $this->get_tmp_file_path('products.json');

    // Start new temp file.
    if (in_array('start', $phase)) {
      $file = $this->file_write($filename, array());
      if (!$file) {
        watchdog('clerk_io', '@filename could not be written. Aborting build products.', array('@filename' => $filename), WATCHDOG_ERROR);
        return;
      }
    }

    // Write new content to the file.
    $data = $this->file_read($filename);

    foreach ($items AS $item) {
      $data[] = (int)$item;
    }

    $this->file_write($filename, $data);

    if (in_array('finish', $phase)) {
      $destination = $this->get_public_file_path('products.json');
      $status = copy($filename, $destination);
      if (!$status) {
        watchdog('clerk_io', '@filename could not be copied to @destination. Destination file was not updated.', array('@filename' => $filename, '@destination' => $destination), WATCHDOG_ERROR);
        return;
      }

      $this->file_destroy();
    }
  }

  /**
   * Build categories JSON.
   *
   * @param $data
   */
  public function buildCategories($data){
    $phase = $data['phase'];
    $items = $data['items'];
    $filename = $this->get_tmp_file_path('categories.json');

    // Start new temp file.
    if (in_array('start', $phase)) {
      $file = $this->file_write($filename, array());
      if (!$file) {
        watchdog('clerk_io', '@filename could not be written. Aborting build categories.', array('@filename' => $filename), WATCHDOG_ERROR);
        return;
      }
    }

    // Write new content to the file.
    $data = $this->file_read($filename);

    $names = db_select('taxonomy_term_data', 'ttd')
      ->fields('ttd', array('tid', 'name'))
      ->condition('tid', $items)
      ->execute()->fetchAllKeyed();

    foreach ($names AS $tid => $name) {
      $subterms = array();
      $children = db_select('taxonomy_term_hierarchy', 'tth')
        ->fields('tth', array('tid'))
        ->condition('parent', $tid)
        ->execute()->fetchCol();

      if (!empty($children)) {
        foreach ($children AS $stid) {
          $subterms[] = (int)$stid;
        }
      }

      $data[] = array(
        'id' => (int)$tid,
        'name' => $name,
        'subcategories' => $subterms,
      );
    }

    $this->file_write($filename, $data);

    if (in_array('finish', $phase)) {
      $destination = $this->get_public_file_path('categories.json');
      $status = copy($filename, $destination);
      if (!$status) {
        watchdog('clerk_io', '@filename could not be copied to @destination. Destination file was not updated.', array('@filename' => $filename, '@destination' => $destination), WATCHDOG_ERROR);
        return;
      }

      $this->file_destroy();
    }

  }

  /**
   * Queue the rebuild of JSON files.
   */
  public function cron_execute() {
    $last_run = variable_get('clerk_io_cron_last_run', 0);
    $always_run = variable_get('clerk_io_cron_always_run', 0);
    $queues = array();

    $run_time = time() - $last_run;

    // Queue orders once a day
    if ($run_time > 86400 || $always_run) {

      // Make sure cron always runs at the time time.
      variable_set('clerk_io_cron_last_run', strtotime(date('Y-m-d') . ' 1:00:00'));

      // Don't queue if something is already in the queue.
      $active_queues = db_query('SELECT name, count(*) FROM {queue} GROUP BY name')->fetchAllKeyed();

      if (!isset($active_queues['clerk_io_order_feed'])) {
        $queues['order'] = $this->queueOrders();
      }

      if (!isset($active_queues['clerk_io_category_feed'])) {
        $queues['category'] = $this->queueCategories();
      }

      if (!isset($active_queues['clerk_io_product_feed'])) {
        $queues['product'] = $this->queueProducts();
      }

    }

    // Set qeueues
    foreach ($queues AS $name => $items) {
      // Skip empty queues
      if (empty($items)) {
        continue;
      }

      // Figure out how many items we need to throw in DrupalQueue.
      $num_operations = 100;
      $loops = count($items) / $num_operations;
      $loops = (int) ceil($loops);

      $queue = new BatchQueue('clerk_io_' . $name . '_feed');

      for ($x = 0; $x < $loops; $x++) {
        // Set phase. This is important because it lets us figure out if we need to
        // create the XML, simply write to it or finalize it
        $phase = array();

        if ($x == 0) {
          $phase[] = 'start';
        }
        if (($x + 1) == $loops) {
          $phase[] = 'finish';
        }

        $slice = array_splice($items, 0, $num_operations);

        $data = array(
          'phase' => $phase,
          'items' => $slice,
        );

        $queue->createItem($data);
      }
    }
  }

  /**
   * Get products attached to an order
   * @param $order
   * @return array
   */
  public function getOrderProducts($order, $full = FALSE) {
    //$order = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $order->commerce_line_items;
    $clerk_products = array();
    $line_item_ids = array();
    foreach ($line_items['und'] AS $line_item) {
      $line_item_ids[] = $line_item['line_item_id'];
    }

    $line_items = commerce_line_item_load_multiple($line_item_ids);

    foreach ($line_items AS $line_item_id => $line_item) {
      if ($line_item->type == 'product' && $full) {
        $product = $line_item->commerce_product['und'][0];
        $pid = $product['product_id'];

        if (!empty($line_item->commerce_total) && !empty($line_item->quantity)) {
          $price = $line_item->commerce_total['und'][0]['amount'] / 100;
          $quantity = $line_item->quantity;

          $clerk_product = new stdClass();
          $clerk_product->id = (int)$pid;
          $clerk_product->quantity = (int)number_format($quantity, 0, '.', '');
          $clerk_product->price = floatval(number_format($price, 2, '.', ''));

          $clerk_products[] = $clerk_product;
        }
        else {
          // Fallback to just supplying the product_id
          $clerk_products[] = (int)$pid;
        }
      }
    }

    return $clerk_products;
  }

  /**
   * Load order as ReadOnly to avoid http://www.drupal.org/node/2240427
   * @param $order_id
   * @return bool|mixed
   */
  protected function commerceOrderLoad($order_id) {
    $order = commerce_order_load($order_id);
    entity_get_controller('commerce_order')->resetCache(array($order->order_id));
    return $order;
  }

  /**
   * Queue orders list.
   *
   * @return array
   */
  protected function queueOrders() {
    // Get all orders
    $query = db_select('commerce_order', 'co')
      ->fields('co', array('order_id'))
      ->condition('status', 'completed');

    if (variable_get('clerk_io_feed_sample', 0)) {
      $query->range(0, 50);
    }

    $orders = $query->execute()->fetchCol();

    if (!empty($orders)) {
      return $orders;
    }

    return array();
  }


  /**
   * Queue product list.
   *
   * @return array
   */
  protected function queueProducts() {
    // Get all commerce product items.
    $query = db_select('commerce_product', 'cp')
      ->fields('cp', array('product_id'))
      ->condition('status', 1)
      ->condition('type', 'product');

    if (variable_get('clerk_io_feed_sample', 0)) {
      $query->range(0, 50);
    }

    $pids = $query->execute()->fetchCol();

    if (!empty($pids)) {
      return $pids;
    }

    return array();
  }

  /**
   * Queue categories.
   *
   * @return array
   */
  protected function queueCategories() {
    $voc = variable_get('clerk_io_category_vocabularies', array());

    if (!empty($voc)) {
      $query = db_select('taxonomy_term_data', 'ttd')
        ->fields('ttd', array('tid'))
        ->condition('vid', $voc);

      if (variable_get('clerk_io_feed_sample', 0)) {
        $query->range(0, 50);
      }

      $terms = $query->execute()->fetchCol();

      if (!empty($terms)) {
        return $terms;
      }
    }

    return array();
  }

  /**
   * Read file from disk.
   *
   * @param $filename
   * @return bool|mixed
   */
  protected function file_read($filename) {
    if (file_exists($filename)) {
      $content = file_get_contents($filename);
      if ($content) {
        $data = drupal_json_decode($content);
        return $data;
      }
    }

    return FALSE;
  }

  /**
   * Write file to disk.
   *
   * @param $filename
   * @param $data
   * @return int
   */
  protected function file_write($filename, $data) {
    return file_put_contents($filename, drupal_json_encode($data));
  }

  /**
   * Delete file.
   *
   * @param string $filename
   * @return bool
   */
  protected function file_destroy($filename = 'product-feed.json') {
    $file = $this->get_public_file_path($filename);
    if (file_exists($file)) {
      return unlink($file);
    }
  }

  /**
   * Get path to file in tmp folder.
   *
   * @param $filename
   * @return string
   */
  protected function get_tmp_file_path($filename) {
    $path =  drupal_realpath('temporary://clerk_io');
    if (!is_dir($path)) {
      mkdir($path, 0775);
    }

    return $path . '/' . $filename;
  }

  /**
   * Get path to file in public folder.
   *
   * @param $filename
   * @return string
   */
  protected function get_public_file_path($filename) {
    $path = drupal_realpath('public://clerk_io');
    if (!is_dir($path)) {
      mkdir($path, 0775);
    }

    return $path . '/' . $filename;
  }
}
