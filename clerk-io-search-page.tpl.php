<span
  id="clerk-search"
  class="clerk"

  data-template="@<?php empty($clerk_template) ? print 'search' : print $clerk_template; ?>"
  data-limit="40"
  data-offset="0"
  data-target="#clerk-search-results"
  data-after-render="_clerk_after_load_event"
  data-query="<?php empty($clerk_query) ? print '' : print $clerk_query; ?>">
</span>

<ul id="clerk-search-results"></ul>
<div id="clerk-search-no-results" style="display: none;"></div>

<button id="clerk-search-load-more-button"><?php t('Load More Results') ?></button>

<script type="text/javascript">
  jQuery('#clerk-search-load-more-button').on('click', function() {
    var e = jQuery('#clerk-search');

    e.data('offset', e.data('offset') + e.data('limit'));

    Clerk.renderBlocks('#clerk-search');
  });

  function _clerk_after_load_event(data) {
    if(data.response.result.length == 0) {
      jQuery('#clerk-search-load-more-button').hide();

      if(jQuery('#clerk-search-results').is(':empty')) {
        jQuery('#clerk-search-no-results').show();
      }
    }
  }
</script>
