<?php

function clerk_io_settings_form($form, &$form_state) {

  $form['clerk_io_tracking_code'] = array(
    '#title' => t('Tracking code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('clerk_io_tracking_code', ''),
    '#description' => t('Get your tracking code from http://my.clerk.io'),
  );

  $form['clerk_io_feed_sample'] = array(
    '#title' => t('Only output sample feed'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('clerk_io_feed_sample', 0),
    '#description' => t('Only 50 elements of each feed element is processed.'),
  );

  $vocabulary_options = array();
  $voc = taxonomy_get_vocabularies();
  foreach ($voc AS $vocabulary) {
    $vocabulary_options[$vocabulary->vid] = $vocabulary->name;
  }

  $form['clerk_io_category_vocabularies'] = array(
    '#title' => t('Enabled vocabularies'),
    '#type' => 'checkboxes',
    '#options' => $vocabulary_options,
    '#default_value' => variable_get('clerk_io_category_vocabularies', array()),
    '#description' => t('Vocabularies to tage terms from for product feed. See http://docs.clerk.io/#/installation/product-feed'),
  );

  $form['clerk_io_run_now'] = array(
    '#title' => t('Run with next cron.'),
    '#type' => 'checkbox',
  );

  $form['#submit'][] = 'clerk_io_settings_form_submit';

  return system_settings_form($form);
}

function clerk_io_settings_form_submit(&$form, &$form_state) {
  if (!empty($form_state['values']['clerk_io_run_now'])) {
    variable_del('clerk_io_cron_last_run');
    drupal_set_message('Clerk IO cron will run next time cron runs');
  }
}
