<?php

$plugin = array(
  'title' => t('Clerk IO: Search results'),
  'description' => t('Adds search results to a page.'),
  'category' => t('Miscellaneous'),
  'defaults' => array(
    'clerk_template' => 'search',
  ),
);

/**
 * Render callback for the plugin.
 */
function clerk_io_search_results_content_type_render($subtype, $conf, $panel_args, $context) {

  $query = !empty($_GET['search_term']) ? $_GET['search_term'] :  '';
  $output =  theme('clerk_io_search_page', array('clerk_template' => $conf['clerk_template'], 'clerk_query' => $query));

  $block = new stdClass();
  $block->content = $output;
  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function clerk_io_search_results_content_type_edit_form($form, &$form_state) {
  $form['clerk_template'] = array(
    '#title' => t('Field class'),
    '#type' => 'textfield',
    '#default_value' => empty($form_state['conf']['clerk_template']) ? 'search' : $form_state['conf']['clerk_template'],
  );

  return $form;
}

/**
 * Submit handler for the custom type settings form.
 */
function clerk_io_search_results_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
