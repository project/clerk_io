<?php
$plugin = array(
  'title' => t('Clerk IO: Live search'),
  'description' => t('Implements live search on the site.'),
  'category' => t('Miscellaneous'),
  'defaults' => array(
    'field_class' => 'clerk-io-live-search',
    'placeholder_text' => NULL,
  ),
);

/**
 * Render callback for the plugin.
 */
function clerk_io_live_search_content_type_render($subtype, $conf, $panel_args, $context) {
  $form = drupal_get_form('clerk_io_live_search_form', $conf['field_class'], $conf['placeholder_text']);

  $block = new stdClass();
  $block->content = render($form);
  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function clerk_io_live_search_content_type_edit_form($form, &$form_state) {
  $form['field_class'] = array(
    '#title' => t('Field class'),
    '#type' => 'textfield',
    '#default_value' => empty($form_state['conf']['field_class']) ? 'clerk-io-live-search' : $form_state['conf']['field_class'],
  );

  $form['placeholder_text'] = array(
    '#title' => t('Placeholder text'),
    '#type' => 'textfield',
    '#default_value' => empty($form_state['conf']['placeholder_text']) ? NULL : $form_state['conf']['placeholder_text'],
  );

  return $form;
}

/**
 * Submit handler for the custom type settings form.
 */
function clerk_io_live_search_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Search form for clerk IO live search.
 */
function clerk_io_live_search_form($form, &$form_state, $field_class='clerk-io-live-search', $placeholder_text = NULL) {

  $form['#action'] = url('search/clerk');
  $form['#method'] = 'GET';

  $form['search_term'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'class' => array($field_class),
      'placeholder' => empty($placeholder_text) ? t('Search') : $placeholder_text,
    ),
    '#default_value' => empty($_GET['search_term']) ? '' : $_GET['search_term'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['clerk_io_markup'] = array(
    '#type' => 'markup',
    '#markup' => '<span
    class="clerk"
    data-template="@live-search"
    data-bind-live-search=".' . $field_class . '">
    </span>'
  );

  return $form;
}
