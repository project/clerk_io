<?php
/**
 * @file
 * Task plugin for ctools
 *   Plugin adds a system page in page manager called search clerk which
 *   overrides the default clerk/search page.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks().
 */
function clerk_io_search_clerk_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('Clerk search template'),

    'admin title' => t('Clerk search template'),
    'admin description' => t('A page for displaying clerk search.'),
    'admin path' => 'search/clerk',

    // Menu hooks so that we can alter the search/clerk menu entry to point to us.
    'hook menu' => 'clerk_io_search_clerk_menu',
    'hook menu alter' => 'clerk_io_search_clerk_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',
    'get arguments' => 'clerk_io_search_clerk_get_arguments',
    'get context placeholders' => 'clerk_io_search_clerk_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('clerk_io_search_clerk_disabled', TRUE),
    'enable callback' => 'clerk_io_search_clerk_enable',
    'access callback' => 'clerk_io_search_clerk_access_check',
  );
}

/**
 * Callback defined by clerk_io_search_clerk_page_manager_tasks().
 */
function clerk_io_search_clerk_menu_alter(&$items, $task) {
  if (variable_get('clerk_io_search_clerk_disabled', TRUE)) {
    return;
  }

  // Override the page callback for our purpose.
  if (isset($items['search/clerk']['page callback'])) {
    $callback = $items['search/clerk']['page callback'];
  }
  else {
    $callback = '';
  }

  if ($callback != 'clerk_io_search_pagemanager_page') {
    $items['search/clerk']['page callback'] = 'clerk_io_search_pagemanager_page';
    if (!empty($task['path'])) {
      $items['search/clerk']['file path'] = $task['path'];
    }
    if (!empty($task['file'])) {
      $items['search/clerk']['file'] = $task['file'];
    }
  }
  else {
    // automatically disable this task if it cannot be enabled.
    variable_set('clerk_io_search_clerk_disabled', TRUE);
    if (!empty($GLOBALS['page_manager_enabling_search_clerk'])) {
      drupal_set_message(t('Page manager module is unable to enable search clerk because some other module already has overridden with %callback.',
        array('%callback' => $callback)), 'error');
    }
  }
}

/**
 * Entry point for our overridden clerk search.
 */
function clerk_io_search_pagemanager_page() {

  // Load my task plugin
  $task = page_manager_get_task('search_clerk');

  // Load the node into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
    return $output;
  }

  return '';
}

/**
 * Callback to get arguments provided by this task handler.
 */
function clerk_io_search_clerk_get_arguments($task, $subtask_id) {
  return array(

  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function clerk_io_search_clerk_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(clerk_io_search_clerk_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function clerk_io_search_clerk_enable($cache, $status) {
  variable_set('clerk_io_search_clerk_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['page_manager_enabling_search_clerk'] = TRUE;
  }
}

/**
 * Callback to determine if a page is accessible.
 *
 * @param $task
 *  The task plugin.
 * @param $subtask_id
 *  The subtask id.
 * @param $contexts
 *  The contexts loaded for the task.
 *
 * @return bool TRUE if the current user can access the page.
 */
function clerk_io_search_clerk_access_check($task, $subtask_id, $contexts) {
  return TRUE;
}
