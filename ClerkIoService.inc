<?php

class ClerkIoService {
  protected $key;
  protected $private_key;

  public function __construct() {
    $this->key = variable_get('clerk_io_tracking_code', '');
    $this->private_key = variable_get('clerk_io_api_private_key', '');
  }

  public function addProduct($id, $name, $description, $price, $data = array()) {

    $path = $this->getApiUrl() . '/product/add';

    $data['key'] = $this->key;
    $data['private_key'] = $this->private_key;
    $data['id'] = $id;
    $data['name'] = $name;
    $data['description'] = $description;
    $data['price'] = $price;

    $options = array();
    $options['method'] = 'POST';
    $options['headers'] = array('Content-Type' => 'application/x-www-form-urlencoded');
    $options['data'] = json_encode($data);


    $response = drupal_http_request($path, $options);

    if ($response->code != 200) {
      throw new ClerkIoServiceException($response->status_message, $response->code);
    }

    return TRUE;

  }

  protected function getApiUrl() {
    return 'https://api.clerk.io/v2';
  }
}

class ClerkIoServiceException extends Exception{}
